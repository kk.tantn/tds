export const state = () => ({
  app_loading: false,
});

export const mutations = {
  setAppLoading(state, loading) {
    state.app_loading = loading;
  },
};
