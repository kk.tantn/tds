import "vanilla-cookieconsent/dist/cookieconsent.css";
import "vanilla-cookieconsent/src/cookieconsent.js";

export default function ({ $config, $gtag }) {
  const cc = initCookieConsent();

  cc.run({
    current_lang: "th",
    // autoclear_cookies: true, // default: false
    cookie_name: "tds_cookie_consent", // default: 'cc_cookie'
    cookie_expiration: 365, // default: 182
    // page_scripts: false,                         // default: false
    // force_consent: true, // default: false
    // auto_language: null,                     // default: null; could also be 'browser' or 'document'
    // autorun: true,                           // default: true
    // delay: 0,                                // default: 0
    // hide_from_bots: false,                   // default: false
    // remove_cookie_tables: false              // default: false
    // cookie_domain: location.hostname,        // default: current domain
    // cookie_path: '/',                        // default: root
    // cookie_same_site: 'Lax',
    // use_rfc_cookie: false,                   // default: false
    // revision: 0,                             // default: 0
    gui_options: {
      consent_modal: {
        layout: "cloud", // box,cloud,bar
        position: "bottom center", // bottom,middle,top + left,right,center
        transition: "slide", // zoom,slide
      },
      settings_modal: {
        layout: "bar", // box,bar
        position: "right", // right,left (available only if bar layout selected)
        transition: "slide", // zoom,slide
      },
    },
    onFirstAction: function () {
      // console.log("onFirstAction fired");
    },
    onAccept: function () {
      //   console.log("onAccept fired!");
      // If analytics category is disabled => load all iframes automatically
      if (cc.allowedCategory("analytics")) {
        // console.log("iframemanager: loading all iframes");
        // manager.acceptService("all");
        $gtag("js", new Date());
        $gtag("config", $config.gtag_id);
      }
    },
    onChange: function (cookie, changed_preferences) {
      //   console.log("onChange fired!");
      // If analytics category is disabled => ask for permission to load iframes
      if (!cc.allowedCategory("analytics")) {
        // console.log("iframemanager: disabling all iframes");
        // manager.rejectService("all");
      } else {
        // console.log("iframemanager: loading all iframes");
        // manager.acceptService("all");
      }
    },
    languages: {
      th: {
        consent_modal: {
          title: "We need your consent",
          description:
            "By clicking “Accept All Cookies”, you agree to the storing of cookies on your device to enhance site navigation, analyze site usage, and assist in our marketing efforts.",
          primary_btn: {
            text: "Accept All Cookies",
            role: "accept_all", //'accept_selected' or 'accept_all'
          },
          secondary_btn: {
            text: "Cookies settings",
            role: "settings", //'settings' or 'accept_necessary'
          },
          //   revision_message:
          //     "<br><br> Dear user, terms and conditions have changed since the last time you visisted!",
        },
        settings_modal: {
          title: "Privacy Preference Center",
          save_settings_btn: "Save",
          accept_all_btn: "Accept All",
          reject_all_btn: "Reject All",
          close_btn_label: "Close",
          cookie_table_headers: [
            { col1: "Name" },
            { col2: "Host" },
            { col3: "Duration" },
          ],
          blocks: [
            {
              //   title: "ตั้งค่าความเป็นส่วนตัว",
              description:
                "When you visit any website, it may store or retrieve information on your browser, mostly in the form of cookies. This information might be about you, your preferences or your device and is mostly used to make the site work as you expect it to. The information does not usually directly identify you, but it can give you a more personalized web experience. Because we respect your right to privacy, you can choose not to allow some types of cookies. Click on the different category headings to find out more and change our default settings. However, blocking some types of cookies may impact your experience of the site and the services we are able to offer.",
            },
            {
              title: "Strictly Necessary Cookies",
              description:
                "These cookies are necessary for the website to function and cannot be switched off in our systems. They are usually only set in response to actions made by you which amount to a request for services, such as setting your privacy preferences, logging in or filling in forms. You can set your browser to block or alert you about these cookies, but some parts of the site will not then work. These cookies do not store any personally identifiable information.",
              toggle: {
                value: "necessary",
                enabled: true,
                readonly: true, //cookie categories with readonly=true are all treated as "necessary cookies"
              },
            },
            {
              title: "Analytics Cookies",
              description:
                "These cookies allow us to count visits and traffic sources so we can measure and improve the performance of our site. They help us to know which pages are the most and least popular and see how visitors move around the site. All information these cookies collect is aggregated and therefore anonymous. If you do not allow these cookies we will not know when you have visited our site, and will not be able to monitor its performance.",
              toggle: {
                value: "analytics",
                enabled: true,
                readonly: false,
              },
              cookie_table: [
                {
                  col1: "^_ga",
                  col2: window.location.origin,
                  col3: "1 Year",
                  is_regex: true,
                },
              ],
            },
            // {
            //   title: "More information",
            //   description:
            //     LOREM_IPSUM +
            //     ' <a class="cc-link" href="https://orestbida.com/contact/">Contact me</a>.',
            // },
          ],
        },
      },
    },
  });
}
